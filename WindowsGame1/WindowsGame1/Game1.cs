using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>


    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Model model;
        Matrix[] modelTransforms;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 500;
            graphics.PreferredBackBufferHeight = 480;
        }
        // Called when the game should load its content
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            model = Content.Load<Model>("ship");
            modelTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(modelTransforms);

        }
        // Called when the game should update itself
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        // Called when the game should draw itself
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            Matrix view = Matrix.CreateLookAt(
            new Vector3(200, 300, 900),
            new Vector3(0, 50, 0),
            Vector3.Up);
            Matrix projection = Matrix.CreatePerspectiveFieldOfView(
            MathHelper.ToRadians(45),
            GraphicsDevice.Viewport.AspectRatio,
            0.1f, 10000.0f);
            // Calculate the starting world matrix
            Matrix baseWorld = Matrix.CreateScale(10.0f) *
            Matrix.CreateRotationY(MathHelper.ToRadians(60))*
            Matrix.CreateRotationZ(MathHelper.ToRadians(50))
            ;
            foreach (ModelMesh mesh in model.Meshes)
            {
                // Calculate each mesh's world matrix
                Matrix localWorld = modelTransforms[mesh.ParentBone.Index]
                * baseWorld;
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    BasicEffect e = (BasicEffect)part.Effect;
                    // Set the world, view, and projection
                    // matrices to the effect
                    e.World = localWorld;
                    e.View = view;
                    e.Projection = projection;
                    e.EnableDefaultLighting();
                }

                // Draw the mesh
                mesh.Draw();
            }
            base.Draw(gameTime);
        }
    }
}
